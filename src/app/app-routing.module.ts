import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainviewComponent } from './component/mainview/mainview.component';
import { ProductComponent } from './component/product/product.component';
import { UserComponent } from './component/user/user.component';

const routes: Routes = [
  { path: '', redirectTo: '/mainview', pathMatch: 'full' },
  { path: 'mainview', component: MainviewComponent },
  { path: 'product', component: ProductComponent },
  { path: 'user', component: UserComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
