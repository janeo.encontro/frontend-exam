import { Component, OnInit } from '@angular/core';

import { MainView } from '../../model/mainview';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-mainview',
  templateUrl: './mainview.component.html',
  styleUrls: ['./mainview.component.css']
})
export class MainviewComponent implements OnInit {
  mainview: MainView;

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.getMainViewConfig();
  }

  getMainViewConfig(): void {
    this.dataService.getMainViewConfig()
      .subscribe(mainview => this.mainview = mainview);
  }
}
