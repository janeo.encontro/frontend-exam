import { Component, OnInit } from '@angular/core';

import { MenuItem } from '../../model/menu-item';
import { Product } from '../../model/product';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  productConfig: MenuItem;

  displayedColumns: string[] = [];
  dataSource: Product[];

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.getProductConfig();
    this.getProductData();
  }

  getProductConfig(): void {
    this.dataService.getProductConfig()
      .subscribe(menuItem => {
        this.productConfig = menuItem;
        this.productConfig.columnFields.forEach(arrayItem => {
          if (arrayItem.isVisble) {
            this.displayedColumns.push(arrayItem.columnName);
          }
        });
      });
  }

  getProductData(): void {
    this.dataService.getProductData()
      .subscribe(productData => this.dataSource = productData);
  }
}
