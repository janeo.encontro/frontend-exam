import { Component, OnInit } from '@angular/core';

import { MenuItem } from '../../model/menu-item';
import { User } from '../../model/user';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  userConfig: MenuItem;

  displayedColumns: string[] = [];
  dataSource: User[];

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.getUserConfig();
    this.getUserData();
  }

  getUserConfig(): void {
    this.dataService.getUserConfig()
      .subscribe(menuItem => {
        this.userConfig = menuItem;
        this.userConfig.columnFields.forEach(arrayItem => {
          if (arrayItem.isVisble) {
            this.displayedColumns.push(arrayItem.columnName);
          }
        });
      });
  }

  getUserData(): void {
    this.dataService.getUserData()
      .subscribe(userData => this.dataSource = userData);
  }

}
