export class MainView {
  name: string;
  menu: [{
    name: string;
    displayName: string;
  }];
  content: string;
}
