export class MenuItem {
  name: string;
  title: string;
  type: boolean;
  columnFields: [{
    columnName: string;
    columnDisplayName: string;
    isVisble: boolean;
    type: string;
  }];
}
