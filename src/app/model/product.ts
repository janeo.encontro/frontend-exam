export class Product {
  productName: string;
  stockCount: string;
  manufacturer: string;
  description: string;
}
