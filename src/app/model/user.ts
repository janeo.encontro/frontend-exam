export class User {
  displayImage: string;
  fullname: string;
  role: string;
  bio: string;
}
