import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { MainView } from '../model/mainview';
import { Product } from '../model/product';
import { MenuItem } from '../model/menu-item';
import { User } from '../model/user';


@Injectable()
export class DataService {
  private apiUrl = 'http://localhost:4200/assets';

  constructor(private http: HttpClient) {
  }

  getMainViewConfig(): Observable<MainView> {
    const url = `${this.apiUrl}/config/mainview.json`;
    return this.http.get<MainView>(url).pipe(
      tap(_ => this.log(`fetched config/mainview.json`)),
      catchError(this.handleError<MainView>(`get config/mainview.json`))
    );
  }

  getProductConfig(): Observable<MenuItem> {
    const url = `${this.apiUrl}/config/products.json`;
    return this.http.get<MenuItem>(url).pipe(
      tap(_ => this.log(`fetched config/products.json`)),
      catchError(this.handleError<MenuItem>(`get config/products.json`))
    );
  }

  getProductData(): Observable<Product[]> {
    const url = `${this.apiUrl}/data/products.json`;
    return this.http.get<Product[]>(url).pipe(
      tap(_ => this.log(`fetched data/products.json`)),
      catchError(this.handleError<Product[]>(`get data/products.json`))
    );
  }

  getUserConfig(): Observable<MenuItem> {
    const url = `${this.apiUrl}/config/users.json`;
    return this.http.get<MenuItem>(url).pipe(
      tap(_ => this.log(`fetched config/users.json`)),
      catchError(this.handleError<MenuItem>(`get config/users.json`))
    );
  }

  getUserData(): Observable<User[]> {
    const url = `${this.apiUrl}/data/users.json`;
    return this.http.get<User[]>(url).pipe(
      tap(_ => this.log(`fetched data/users.json`)),
      catchError(this.handleError<User[]>(`get data/users.json`))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }

}
